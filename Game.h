#include "TextureManager.h"
#include "Map.h"
#include "Player.h"

#define SCR_WIDTH 1200
#define SCR_HEIGHT 1200

#define ORTHO_X 600
#define ORTHO_Y 600

enum GameState {
	START,
	PLAY
};
class Game
{
public:
	Game();
	~Game();

	void loadGameObjects();
	void gameLoop();

	Player* getPlayer() { return m_player; }

	float oldTimeSinceStart = 0;
	float timeSinceStart = 0;
	float deltaTime = 0;

private:
	Map* m_map;
	Player* m_player;

	bool m_gameOver;

	GameState m_gameState;

};

