#ifndef _TextureManager_H_
#define _TextureManager_H_
#include <GL/glew.h>
#include <GL/glut.h>
#include <iostream>

typedef unsigned int uint;

class TextureManager {
public:
	static TextureManager& getInstance()
	{
		static TextureManager instance;
		return instance;
	}

	uint loadTexture(GLenum target, GLenum wrapping, const char* texturePath, bool flip, int channels);

	void drawSprite(unsigned int texture, int length, int height, float angle);
	void rgb(float r, float g, float b);

private:
	static TextureManager* myInstance;
};

#endif