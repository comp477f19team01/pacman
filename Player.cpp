#include "Player.h"

Player::Player() : m_angle(0.0f), m_dir(None), m_xOffset(8.0f), m_speed(0.007), m_lives(3)
{
	m_currentTexture = TextureManager::getInstance().loadTexture(GL_TEXTURE_2D, GL_REPEAT, "Assets/pacman.png", false, 4);

	m_xPos = 13.5f;
	m_yPos = 7.f;
}


Player::~Player()
{
}

void Player::drawPlayer(Map* map, float deltaTime)
{
	glPushMatrix();

	//translate pacman to map's origin
	map->translateMapOrigin();

	map->translateMapToCoords(m_xPos, m_yPos);

	TextureManager::getInstance().drawSprite(m_currentTexture, 16, 16, m_angle);

	glPopMatrix();
}

void Player::setDirection(unsigned char key)
{
	if (key == GLUT_KEY_LEFT)
	{
		m_dir = Left;
	}
	else if (key == GLUT_KEY_RIGHT)
	{
		m_dir = Right;
	}
	else if (key == GLUT_KEY_DOWN)
	{
		m_dir = Down;
	}
	else if (key == GLUT_KEY_UP)
	{
		m_dir = Up;
	} 
}

void Player::movePlayer(Map* map, float deltaTime)
{
	//Check if pacman is near a wall when pacman is in the center of a tile
	if (checkAtTileCenter())
	{
		//when the player tries to move in a direction and there is a wall,
		//set direction to None so that the player stops moving
		if (map->checkIfWall(checkNextTile(map, m_dir)))
		{
			m_dir = None;
		}
	}

	//move the player if the necessary direction when there is a direction to move to
	if (m_dir != None)
	{
		if (m_dir == Left)
		{
			m_xPos -= m_speed * deltaTime;
			m_yPos = round(m_yPos);
			m_angle = 0.0f;
		}
		else if (m_dir == Right)
		{
			m_xPos += m_speed * deltaTime;
			m_yPos = round(m_yPos);
			m_angle = 180.f;
		}
		else if (m_dir == Down)
		{
			m_yPos -= m_speed * deltaTime;
			m_xPos = round(m_xPos);
			m_angle = -270.f;
		}
		else if (m_dir == Up)
		{
			m_yPos += m_speed * deltaTime;
			m_xPos = round(m_xPos);
			m_angle = 270.f;
		}
	}

}

//checks the next tile based on the direction the player is facing
TileType Player::checkNextTile(Map* map, Direction dir)
{
	if (dir == Up)
	{
		return map->getTile(int(getXPos()), int(getYPos()) + 1);
	}
	else if (dir == Down)
	{
		return map->getTile(int(getXPos()), int(getYPos()) - 1);
	}
	else if (dir == Left)
	{
		return map->getTile(int(getXPos()) - 1, int(getYPos()));
	}
	else if (dir == Right)
	{
		return map->getTile(int(getXPos()) + 1, int(getYPos()));
	}
	else
	{
		map->getTile(getXPos(), getYPos());
	}
}


//checks if the player is in the center of a tile
bool Player::checkAtTileCenter()
{
	return (int)round(m_yPos * 10.0f) % 10 == 0 && (int)round(m_yPos * 10.0f) % 10 == 0;
}

