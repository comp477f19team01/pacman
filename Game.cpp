#include "Game.h"

Game::Game() : m_gameOver(false), m_map(nullptr), m_player(nullptr)
{

}


Game::~Game()
{
	delete m_player;
	delete m_map;
}

void Game::loadGameObjects()
{
	m_gameState = GameState::PLAY;

	m_map = new Map();
	m_player = new Player();
}

void Game::gameLoop()
{
	timeSinceStart = float(glutGet(GLUT_ELAPSED_TIME));
	deltaTime = timeSinceStart - oldTimeSinceStart;
	oldTimeSinceStart = timeSinceStart;

	m_player->movePlayer(m_map, deltaTime);

	m_map->drawMap();
	m_player->drawPlayer(m_map, deltaTime);

	glutPostRedisplay();
}
