#include "Game.h"

Game* game = new Game();

void display(void);
void keyboard(unsigned char key, int x, int y);
void handleInput(int key, int, int);
void visibility(int visibility);

int main(int argc, char* argv[])
{
	//Initialize glut settings
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_DOUBLE | GLUT_RGBA);
	glutInitWindowSize(SCR_WIDTH, SCR_HEIGHT);
	glutInitWindowPosition(50, 50);
	glutCreateWindow("PacMan");

	glutDisplayFunc(display);

	glutVisibilityFunc(visibility);

	glewExperimental = TRUE;
	GLenum err = glewInit();
	if (GLEW_OK != err)
		std::cout << "no glew" << std::endl;

	glEnable(GL_BLEND);
	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();
	gluOrtho2D(0, ORTHO_X, 0, ORTHO_Y);

	//set background color of the screen
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);

	game->loadGameObjects();

	glutMainLoop();
	
	delete game;
	return 0;
}

void display()
{
	glClear(GL_COLOR_BUFFER_BIT);   // Clear display buffer colour
	glMatrixMode(GL_MODELVIEW);     // Set matrix mode - no further projection is required in this 2D game
	glLoadIdentity();

	//handle the game input
	glutKeyboardFunc(keyboard);
	glutSpecialFunc(handleInput);

	//process the game
	game->gameLoop();

	glutPostRedisplay();

	glutSwapBuffers();
}

void keyboard(unsigned char key, int x, int y)
{
	//key 27 = escape
	if (key == 27)
		exit(0);
}

void handleInput(int key, int, int)
{
	game->getPlayer()->setDirection(key);
}

void visibility(int visibility)
{
	
}
