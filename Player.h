#pragma once
#include "TextureManager.h"
#include "Map.h"

enum Direction {
	Up,
	Down, 
	Left, 
	Right,
	None
};

class Player
{
public:
	Player();
	~Player();

	void setDirection(unsigned char key);
	void movePlayer(Map* map, float deltaTime);
	void drawPlayer(Map* map, float deltaTime);
	TileType checkNextTile(Map* map, Direction dir);

	bool checkAtTileCenter();

	//returns the tile location of the player on the map
	float getXPos() { return round(m_xPos); }
	float getYPos() { return round(m_yPos); }

private:

	float m_xPos, m_yPos;
	float m_angle;
	float m_xOffset;
	float m_speed;
	
	int m_lives;

	Direction m_dir;
	Direction m_tempDir;
	uint m_currentTexture;
	uint m_pacman_1 = 0;
	uint m_pacman_2 = 0;
	uint m_pacman_3 = 0;

};

